 #!/usr/bin/python
  # -*- coding: latin-1 -*-
 # -*- coding: ascii -*-
 # -*- coding: utf-8 -*-
import PIL.Image as NewImage
import datetime
from PIL import ImageStat
from PIL import Image as img
import pyodbc
import urllib
import operator
import math
import cv2
import socks
import socket
import urllib2
import cookielib
import httplib
import re
import gcs_oauth2_boto_plugin
from urlparse import urlparse
import wand
from wand import image
from wand.image import Image
from wand.display import display
import sys
import boto
import os
import shutil
import tempfile
from dateutil.relativedelta import relativedelta
import time
import PIL

###### Google Cloud Storage Parameters #######

GOOGLE_STORAGE = 'gs'
# URI scheme for accessing local files.
LOCAL_FILE = 'file'

#insert client id and client_secret from console.developers.google.com
CLIENT_ID = '224262177128-4ihhugedtveej5q436jh3rt90avtuig1.apps.googleusercontent.com'
CLIENT_SECRET = 'sFtAvvIEMhXkAz_wfyhh4m0l'
project_id = 'festive-avenue-355'
BUCKET_NAME= 'easy/'
DIR='images/PICS/'

cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))

###### SQL Queries #######

conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.0.201;DATABASE=spiders;UID=sa;PWD=redwop')
cursor = conn.cursor()

###### Temporary Parameters #######

#create temp folder


UNAVAILABLE_URL = []

def upload_file_to_bucket(path_to_dir, bucket_name, Google):
    """
    :param path_to_dir: temporary directory of pic
    :param bucket_name: bucket to upload files

    this function uploads all the files that was created in download_and_save function into google cloud storage
    and give them read permission to allUsers by command prompt
    """
    path = path_to_dir
    source = os.path.join(Google, path)
    filelist = []  # create temp filelist that will contain all files
    try:
        # find all files, dirs that are in the path
        for dirname, dirnames, filenames in os.walk(source):
            for filename in filenames:
                filelist.append(os.path.join(dirname, filename))
    except IOError:
        print ("I/O Error")
    except Exception:
        print ("Unknown error")
        sys.exit(1)
    for filename in filelist:
        cloud_storage_name = bucket_name + '/' + filename.replace(path, '')
        with open(filename, 'rb') as localfile:
            try:
                #upload the files to the cloud
                dst_uri = boto.storage_uri(cloud_storage_name, GOOGLE_STORAGE)
                time.sleep(2)
                dst_uri.new_key().set_contents_from_file(localfile)
                print ('Successfully created "%s/%s"' % (
                    dst_uri.bucket_name, dst_uri.object_name))
                os.system('gsutil acl ch -u AllUsers:R gs://' + cloud_storage_name)
                os.system('gsutil setmeta -h "Cache-Control:public, max-age=10800"  gs://' + cloud_storage_name)
            except Exception:
                print ("File upload error: ")

def resize_pic(height , width , size , img , thumbnail, GCS):
    """
    this function resize and save the pic
    if the shape of the pic is square - the function resize the pic to specific size (250*250 - regular pic , 600*600- big pic)
    if the shape of the image is Rectangle - the function calculate thr ratio between height and width and resize the pic

    return: size of the new image
    """
    if (height == width):
        img.resize(size, size)
    elif (height > width):
        ratio = (float(width) / float(height))
        new_width = int(size * ratio)
        img.resize(new_width, size)
    else:
        ratio = (float(height) / float(width))
        new_height = int(size * ratio)
        img.resize(size, new_height)
    img.save(filename= GCS + thumbnail)
    return os.stat(GCS + thumbnail).st_size

def blurry_image(image):
    """"
    checks if img has one color/blurry/low quality by using OpenCV function

    return: value of blurry
    """
    try:
        img1 = img.open(image)
        pic = cv2.imread(image)
        gray_image = cv2.cvtColor(pic, cv2.COLOR_BGR2GRAY)
        blurry = cv2.Laplacian(gray_image, cv2.CV_64F).var()
        if blurry <= 100:
            print ('blurry img')
    except:
        # bad img
        blurry = 0
    return blurry

def pic_component(img, image):
    """
    :param img: Wand object
    :param image: regular image
    :return: width, height, resolution and size of img
    """
    width = img.width
    height = img.height
    resolution = str(width) + 'x' + str(height)
    size = os.stat(image).st_size
    return width, height, resolution, size











