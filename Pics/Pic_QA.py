 #!/usr/bin/python
  # -*- coding: latin-1 -*-
 # -*- coding: ascii -*-
 # -*- coding: utf-8 -*-

import pyodbc
import gcs_oauth2_boto_plugin
import subprocess
import os
import time
import shutil
###### Google Cloud Storage Parameters #######

GOOGLE_STORAGE = 'gs'
# URI scheme for accessing local files.
LOCAL_FILE = 'file'

#insert client id and client_secret from console.developers.google.com
CLIENT_ID = '224262177128-4ihhugedtveej5q436jh3rt90avtuig1.apps.googleusercontent.com'
CLIENT_SECRET = 'sFtAvvIEMhXkAz_wfyhh4m0l'
project_id = 'festive-avenue-355'
gcs_oauth2_boto_plugin.SetFallbackClientIdAndSecret(CLIENT_ID, CLIENT_SECRET)
BUCKET_NAME= 'easy/'
DIR='images/bizpic/'

conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.0.201;DATABASE=spiders;UID=sa;PWD=redwop')
cursor = conn.cursor()

sql = "select id,width,height,picture_resolution from NewMedia where Data_Type = 'Pic' order by newid()"
job_run = "exec spiders.dbo.Check_If_Job_Is_Runing @job_name='Easy_Update'"
SpiderManager= "select KillProcess from SpidersManager where ComputerIP = '192.168.0.10'"

#sql_test = "select id,width,height,picture_resolution from NewMedia where id =710099"

def check_acl(name):
    # checks if image has access_control: READ to AllUsers (Public img)
    access_control = subprocess.check_output('gsutil acl get gs://easy/images/PICS/' + name, shell=True)
    if access_control.find("allUsers") == -1:
        os.system('gsutil acl ch -u AllUsers:R gs://easy/images/PICS/' + name)
        print ('Successfully updated access_control to ' +str(name))

def exists_in_cloud(name):
    # checks if image exists in easy/images/PICS/
    try:
        exist = subprocess.check_output('gsutil ls -l gs://easy/images/PICS/' + name, shell=True)
    except:
        exist = "Exception"
        print ('not exists in Google Cloud Storage: ' +str(name))
    return exist

def check_file(sql):
    # checks if image exists in GCS - yes : checks access control, no : updates DB that the crawler need to check this image again
    try:
        for row in cursor.execute(sql).fetchall():
            check = cursor.execute(SpiderManager).fetchall()
            if check[0][0] == False:
                print ('NewMedia.id = ' + str(row[0]))
                thumb = str(row[0]) +'.jpg'
                exist_big = "hasnt checked"
                exist = "hasnt checked"
                height = row[2]
                width = row[1]
                if width == None or height == None:
                    pic_size = row[3].encode('UTF-8', 'ignore')
                    width = int(pic_size.split("x")[0])
                    height = int(pic_size.split("x")[1])
                #if(height >= 400 or width >= 400):
                #    big_pic = str(row[0]) +'_big_pic.jpg'
                #    exist_big = exists_in_cloud(big_pic)
                if exist_big.find("Exception") == -1:
                    exist = exists_in_cloud(thumb)
                if exist.find("Exception") != -1 or exist_big.find("Exception") != -1:
                    cursor.execute("update NewMedia set Data_Type = NULL, Size_in_Bytes = 0, InsertDate = NULL, Checked_Date = NULL, Check_Again_Date =NULL where id = ?", row[0])
                    conn.commit()
                else:
                    check_acl(thumb)
                    #if exist_big.find("hasnt checked") == -1:
                    #    check_acl(big_pic)
            else:
                time.sleep(1800)
    except:
        print ("not connected to sql")


def main():
    check = cursor.execute(SpiderManager).fetchall()
    while True:
        if check[0][0] == False:
            os.system('gsutil ls gs://easy/images/PICS/10000*')
            check_file(sql)
        else:
            time.sleep(1800)


main()