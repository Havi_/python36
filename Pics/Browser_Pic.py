 #!/usr/bin/python
  # -*- coding: latin-1 -*-
 # -*- coding: ascii -*-
 # -*- coding: utf-8 -*-
import PIL.Image as NewImage
import datetime
import pyodbc
import urllib
import operator
import math
import cv2
import socks
import socket
import urllib2
import cookielib
from PIL import ImageStat
from PIL import Image as img
import httplib
import re
import gcs_oauth2_boto_plugin
from urlparse import urlparse
import wand
from wand import image
from wand.image import Image
from wand.display import display
import sys
import boto
import os
import shutil
import tempfile
from dateutil.relativedelta import relativedelta
import time
import PIL
import schedule
from Function import upload_file_to_bucket,resize_pic,pic_component,blurry_image
from os import listdir
from os.path import isfile, join

###### Google Cloud Storage Parameters #######

GOOGLE_STORAGE = 'gs'
# URI scheme for accessing local files.
LOCAL_FILE = 'file'

#insert client id and client_secret from console.developers.google.com
CLIENT_ID = '224262177128-4ihhugedtveej5q436jh3rt90avtuig1.apps.googleusercontent.com'
CLIENT_SECRET = 'sFtAvvIEMhXkAz_wfyhh4m0l'
gcs_oauth2_boto_plugin.SetFallbackClientIdAndSecret(CLIENT_ID, CLIENT_SECRET)
project_id = 'festive-avenue-355'
BUCKET_NAME= 'easy/'
DIR='images/PICS/'

Path = "Z:\Inetpub\easy100\images\CloudPic\\"
pathToStatic = 'Z:\Inetpub\easy100\images\staticLogo\\'

cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))

###### SQL Queries #######

job_run = "exec spiders.dbo.Check_If_Job_Is_Runing @job_name='Easy_Update'"
SpiderManager= "select KillProcess from SpidersManager where ComputerIP = '192.168.0.10'"
conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.0.201;DATABASE=spiders;UID=sa;PWD=redwop')
cursor = conn.cursor()
sql_all = "select top 300 url_of_data ,max(date) as insertdate, NewMedia.id from NewMedia where  NewMedia.Data_Type is null and url_of_data not like '%www.d.co.il%' and url_of_data not like '%.zap.co.il%' and url_of_data not like'%rest.co.il%' and url_of_data not like'%zapweb.co.il%' and url_of_data not like '%maps.googleapis.com%' group by url_of_data,NewMedia.id order by newid()"
#sql_facebook="select url_of_data ,max(date) as insertdate, NewMedia.id from newmedia where Data_Type is null and SUBSTRING(url_of_data,0,CHARINDEX('?',url_of_data,0)) in (select SUBSTRING(datavalue,0,CHARINDEX('?',datavalue,0)) from datas where DataName='pic' and DataValue like 'https://fbcdn-profile-a.akamaihd.net/%' )and url_of_data like 'https://fbcdn-profile-a.akamaihd.net/%' group by url_of_data,NewMedia.id"
#sql_update = "select DataValue ,max(datas.InsertDate) as InserDate, NewMedia.id from datas, NewMedia where DataName = 'Pic' and datas.DataValue=NewMedia.url_of_data and NewMedia.Data_Type is null and url_of_data not like '%www.d.co.il%' and url_of_data not like '%.zap.co.il%' and url_of_data not like'%rest.co.il%' and url_of_data not like'%zapweb.co.il%' and url_of_data not like '%maps.googleapis.com%' group by DataValue,NewMedia.id order by newid()"
#static ="select url_of_data, StaticDatas.InsertDate,NewMedia.id from StaticDatas,NewMedia where DataName = 'Pic' and NewMedia.Data_Type is Null and 'http://easy.co.il/tempuploads/' + DataValue = NewMedia.url_of_data order by newid()"
#staticlogos= "select BizId,DataValue,InsertDate from staticdatas where dataname = 'logo'and datavalue not in (select url_of_data from NewMedia where Data_Type='logo') and insertdate > (select Max(insertdate) from NewMedia where Data_Type='logo')"
staticlogos= "select BizId,DataValue,InsertDate from staticdatas where dataname = 'logo'and insertdate > (select top 1 InsertDate from NewMedia where Data_Type = 'logo' order by id desc)"
###### Temporary Parameters #######

#create temp folder
GCS = tempfile.mkdtemp(prefix = 'GCS')
GCS = GCS + '\\'
Google = tempfile.mkdtemp(prefix = 'googlestorage')

h1 = NewImage.open('Market.jpg').histogram()
h3 = NewImage.open('Error.jpg').histogram()

def valid_pic_url(s):
    """
    this function creates temp image file according to the file extension (jpg, gif, png or jpeg)
    then it try to open the url by if using Magic Browser for user agent.
    if it success, it try to open the url as an Image file
    if it failed, the function return -1. otherwise, return regular file and image file
    """
    try:
        cj.clear()
        r = opener.open(s) # instead of urlopen
        image = 'temp2.jpg'
        f = open(image, 'wb')
        f.write(r.read()) # instead of urlopen
        f.close()
        i = Image(filename = image)
        if i.format == 'GIF' or i.format == 'gif':
            i = Image(i.sequence[0]).clone() # first frame
        else:
            if s.find('http://37.19.113.41/tempuploads/') ==-1:
                if blurry_image(image) < 100:
                    f = -2
                    image = -2
        try:
            if f != -2 and image !=- 2:
                h2 = NewImage.open(image).histogram()
                rms = math.sqrt(reduce(operator.add,map(lambda a,b: (a-b)**2, h1, h2))/len(h1))
                rms1 = math.sqrt(reduce(operator.add,map(lambda a,b: (a-b)**2, h3, h2))/len(h3))
                if rms == 0.0 or rms1 == 0.0:
                    f = -1
                    image = -1
                    print ("the same pic")
        except:
            print ('rms exception')
        return f,image
    except:
        # unavailable url
        return -1,-1

def download_and_save(thumb, s,Insert_Date):
    """
    this function finds all the pic's parameters and insert them to the sql table - Pic_Datas.
    it also check if the pic is small, regular or big. if it small the function return 0, regular - 1 and big - 2
    and if the url is unavailable, the function returns -1
    """
    f,image = valid_pic_url(s)
    if image !=- 1 and image !=- 2:
        try:
            with Image(filename = image) as i:
                count_pic = 1 # 1 - marks regular pic, 2- marks big pic
                if i.format == 'GIF' or i.format == 'gif':
                    i = Image(i.sequence[0]).clone() # first frame
                width, height, resolution, size = pic_component(i,image)
                now = datetime.datetime.today()
                new_img = i.clone()
                if (height >= 100 or width >= 100):
                    resize_pic(height,width,250,new_img,str(thumb) + '.jpg',GCS)
                    #if(height >= 400 or width >= 400): # save big_pic
                    #    resize_pic(height,width,600,i,str(thumb) + '_big_pic.jpg',GCS)
                    #    count_pic=2
                    cursor.execute("update NewMedia set Data_Type = ? , Size_in_Bytes = ? ,width = ?, height = ?, InsertDate = ? , Checked_Date = ? where id = ? " ,'Pic', size,width,height ,Insert_Date , now , thumb)
                    conn.commit()
                    return count_pic

                else:
                    # small pic
                    cursor.execute("update NewMedia set Data_Type = ?, Size_in_Bytes = ?, InsertDate = ?, Checked_Date = ? where id = ?" ,'Small_Pic', size ,Insert_Date , now , thumb)
                    conn.commit()
                    return 0
        except:
            # unavailable pic
            return -1
    else:
        # unavailable url
        return image

def upload_and_delete(thumb,newStr,Insert_Date,db_url):
    """
    :param thumb: thumbnail
    :param newStr: url
    :param newStr: Insert Date
    :return: the function return new thumbnail

    the function uploads all pics that was created at download_and_save function from GCS directory.
    if the num_of_pic = 2 it create 2 pics (big and regular), if the num_of_pic = 1 it creates only regular pic
    in both of the cases, the function delete the pics from the computer after save them in the Google Cloud Storage

    if the num_of_pic = 0 marks small pic
    if num_of_pic = -1 marks unavailable_url.
    """
    print ('time =' +str(datetime.datetime.today()) + ' , id = ' + str(thumb) + ', link = ' + newStr)
    num_of_pic = download_and_save(thumb, newStr,Insert_Date)
    if num_of_pic > 0:
        upload_file_to_bucket(GCS, 'easy/images/PICS',Google)
        #if num_of_pic == 2:
        #     os.remove(GCS + str(thumb) + '_big_pic.jpg')
        os.remove(GCS + str(thumb) + '.jpg')
        return
    elif num_of_pic == 0:
        return
    else:
        print ('UNAVAILABLE_URL')
        now = datetime.datetime.today()
        if num_of_pic == -1:
            data = 'unavailable_url'
        else:
            data = 'blurry_pic'
        cursor.execute("update NewMedia set Data_Type = ?, Size_in_Bytes = ?, InsertDate = ?, Checked_Date = ? where ID = ?" ,data, '' ,Insert_Date ,now , thumb)
        conn.commit()
        return thumb

def insert_datas_to_newmedia_table(sql):
    """
    :param sql: marks NewMedia table in DB
    this function call finds all the urls in NewMedia and call upload_and_delete function.
    """
    try:
        for row in cursor.execute(sql).fetchall():
            check = cursor.execute(SpiderManager).fetchall()
            if check[0][0] == False:
                db_url = row[0].encode('UTF-8', 'ignore')
                newStr = (urllib.quote(db_url,safe="%/:=&?~#+!$,;'@()*[]" )).decode("utf8")
                if newStr.find("http://easy.co.il/tempuploads/") != -1:
                    newStr = newStr.replace("easy.co.il","37.19.113.41")
                Insert_Date = row[1]
                upload_and_delete(row[2],newStr,Insert_Date,db_url)
            else:
                time.sleep(3000)
    except:
        print ("not connected to sql")

def update_sql(data, size, now,thumb,regular_size, big_size,width,height):
    """
    update all the img's parameters in DB
    """
    cursor.execute("update NewMedia set Data_Type = ?,  Size_in_Bytes = ? ,Checked_Date = ?, file_size_120 = ? , file_size_250 = ?, width = ?, height = ?  where id = ?" ,data, size , now , regular_size, big_size,width,height ,int(thumb[0]))
    conn.commit()

def upload_img():
    """
    the function uploads all the pics that was created at download_and_save function from GCS directory.
    if the num_of_pic = 2 it creates 2 pics (big and regular), if the num_of_pic = 1 it creates only regular pic
    in both of the cases, the function delete the pics from the computer after save them at Google Cloud Storage
    """
    check = cursor.execute(SpiderManager).fetchall()
    if check[0][0] == False:
        try:
            images = os.listdir(Path)
            for img in images:
                thumb = img.split('.')
                if thumb[0].isdigit() and thumb[0]!='3224903' and thumb[0]!='3224906' and thumb[0]!='3224879':
                    try:
                        i = Image(filename = Path +'/' + img)
                        if i.mimetype == 'image/gif':
                            i = i.sequence[0].clone() # first frame
                            os.remove(Path +'/' + img)
                            i.save(filename = Path +'/' + img)
                        blurry = blurry_image(Path +'/' + img)
                        now = datetime.datetime.today()
                        if blurry <= 100:
                            update_sql('blurry_pic', 0, now,thumb, None, None,None,None)
                            print ('blurry_pic')
                        else:
                            width, height, resolution, size = pic_component(i,Path +'/' + img)
                            new_img = i.clone()
                            if (height >= 100 or width >= 100):
                                regular_size = resize_pic(height,width,250,new_img, img, GCS)
                                big_size = None
                                num_of_pic = 1
                                #if(height >= 400 or width >= 400): # save big_pic
                                #    big_size = resize_pic(height,width,600,i, thumb[0] + '_big_pic.jpg', GCS)
                                #    num_of_pic = 2
                                update_sql('Pic', size, now,thumb,regular_size, big_size,width, height)
                                upload_file_to_bucket(GCS, 'easy/images/PICS',Google)
                            else:
                                # small pic
                                update_sql('Small_Pic', size, now,thumb, None, None,width, height)
                                num_of_pic = 0
                            if num_of_pic >= 1:
                                os.remove(GCS + img)
                                if num_of_pic == 2:
                                    os.remove(GCS + '/' +thumb[0] + '_big_pic.jpg')
                    except:
                       print("didn't find the pic")
                    os.remove(Path +'/' + img)
        except:
            print ("didn't find the pic")
    else:
        time.sleep(3000)

def upload_static_logo():
    try:
        for row in cursor.execute(staticlogos).fetchall():
            check = cursor.execute(SpiderManager).fetchall()
            if check[0][0] == False:
                filename = pathToStatic + row[1] + '.jpg'
                try:
                    i = Image(filename = filename)
                    i.save(filename= GCS + row[1] + '.jpg')
                    upload_file_to_bucket(GCS, 'easy/images/StaticLogo', Google)
                    cursor.execute("insert into NewMedia(data_owner,url_of_data,picture_type,Bizid,Data_Type,InsertDate) Values(?,?,?,?,?,?)" ,('PicLogo',row[1],'logo',row[0],'logo',row[2]))
                    conn.commit()
                    os.remove(GCS + row[1] + '.jpg')
                except:
                    print ('not an img')
            else:
                time.sleep(3000)
    except:
        print ('not connect to sql')

def main():
    while True:
        check = cursor.execute(SpiderManager).fetchall()
        if check[0][0] == False:
            os.system('gsutil ls gs://easy/images/PICS/10000*')
            #upload_static_logo()
            upload_img()
            #insert_datas_to_newmedia_table(static)
            #insert_datas_to_newmedia_table(sql_update)
            #insert_datas_to_newmedia_table(sql_facebook)
            insert_datas_to_newmedia_table(sql_all)
            time.sleep(1000)
        else:
            time.sleep(3000)
    shutil.rmtree(GCS) #delete temp folder
    shutil.rmtree(Google)

main()

def static():
    for f in listdir(pathToStatic):
        try:
            i = Image(filename = pathToStatic+ f)
            i.save(filename= GCS + f)
            upload_file_to_bucket(GCS, 'easy/images/StaticLogo', Google)
            os.remove(GCS + f)
        except:
            print ("browser pic field")

#static()







