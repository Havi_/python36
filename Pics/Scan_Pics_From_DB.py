 #!/usr/bin/python
  # -*- coding: latin-1 -*-
 # -*- coding: ascii -*-
 # -*- coding: utf-8 -*-
import PIL.Image as NewImage
import datetime
import gcs_oauth2_boto_plugin
from PIL import ImageStat
from PIL import Image as img
import pyodbc
import urllib
import operator
import math
import socks
import socket
import urllib2
import cookielib
import httplib
import re
import gcs_oauth2_boto_plugin
from urlparse import urlparse
import wand
from wand import image
from wand.image import Image
from wand.display import display
import sys
import boto
import os
import StringIO
import shutil
import tempfile
from dateutil.relativedelta import relativedelta
import time
import PIL
from Function import upload_file_to_bucket,resize_pic,pic_component,blurry_image

###### Google Cloud Storage Parameters #######

GOOGLE_STORAGE = 'gs'
# URI scheme for accessing local files.
LOCAL_FILE = 'file'

#insert client id and client_secret from console.developers.google.com
CLIENT_ID = '224262177128-4ihhugedtveej5q436jh3rt90avtuig1.apps.googleusercontent.com'
CLIENT_SECRET = 'sFtAvvIEMhXkAz_wfyhh4m0l'
gcs_oauth2_boto_plugin.SetFallbackClientIdAndSecret(CLIENT_ID, CLIENT_SECRET)
project_id = 'festive-avenue-355'
BUCKET_NAME= 'easy/'
DIR='images/PICS/'

cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))

###### SQL Queries #######

conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.0.201;DATABASE=spiders;UID=sa;PWD=redwop')
cursor = conn.cursor()

#staticDatas = "select distinct ('http://easy.co.il/tempuploads/' + DataValue) as url_of_data, InsertDate from StaticDatas where ('http://easy.co.il/tempuploads/' + DataValue) not in (select url_of_data from newmedia where data_owner = 'PicUser') and DataName = 'Pic' and DataValue not like '0' and DataType = 1 "
#datas = "select DataValue,max(InsertDate) from datas where DataValue not in (select url_of_data from newmedia) and DataName = 'Pic' and datavalue not like '%www.t.co.il%' and DataValue like '%http%' and datas.id not in (260108060,260098406,264365409) group by DataValue"
datas = "exec [dbo].[GetMediaURLWithoutFB]"

job_run = "exec spiders.dbo.Check_If_Job_Is_Runing @job_name='Easy_Update'"
SpiderManager= "select KillProcess from SpidersManager where ComputerIP = '192.168.0.10'"
###### Temporary Parameters #######

#create temp folder
GCS = tempfile.mkdtemp(prefix = 'GCS')
GCS = GCS + '\\'
Google = tempfile.mkdtemp(prefix = 'googlestorage')

h1 = NewImage.open('Market.jpg').histogram()
h3 = NewImage.open('Error.jpg').histogram() #BankHapoalim Error

def valid_pic_url(s,data_owner):
    """
    this function creates temp image file according to the file extension (jpg)
    then it try to open the url of the img:
        if it success, it tries to open the url as an Image file and check if the image is blurry or has Error (Error.jpg)
        if it failed, the function return -1. otherwise, return regular file and image file
    """
    try:
        cj.clear()
        r = opener.open(s,None,50) # instead of urlopen
        image = 'temp.jpg'
        f = open(image, 'wb')
        f.write(r.read()) # instead of urlopen
        f.close()
        i = Image(filename = image)
        if i.format == 'GIF' or i.format == 'gif':
            i = Image(i.sequence[0]).clone() # first frame
        else:
            if data_owner.find("PicUser")== -1:
                if blurry_image(image) <= 100:
                    f = -2
                    image = -2
        try:
            if f != -2 and image !=- 2:
                h2 = NewImage.open(image).histogram()
                rms = math.sqrt(reduce(operator.add,map(lambda a,b: (a-b)**2, h1, h2))/len(h1))
                rms1 = math.sqrt(reduce(operator.add,map(lambda a,b: (a-b)**2, h3, h2))/len(h3))
                if rms == 0.0 or rms1 == 0.0:
                    f = -1
                    image = -1
                    print ("the same pic")
        except:
            'rms exception'
        return f,image
    except:
        # unavailable url
        return -1,-1

def download_and_save(s,Insert_Date,db_url):
    """
    this function finds all the img's parameters and insert them to the sql table - NewMedia.
    it also checks if the img is small, regular or big. if it small the function return 0, regular - 1 and big - 2
    and if the url is unavailable, the function returns -1
    """
    data_owner = 'PicSpider'
    if s.find("http://37.19.113.41/tempuploads")!= -1:
        data_owner = 'PicUser'
    f,image = valid_pic_url(s,data_owner)
    if(image) !=- 1:
        try:
            with Image(filename = image) as i:
                count_pic = 1 # 1 - marks regular pic, 2- marks big pic
                if i.format == 'GIF' or i.format == 'gif':
                    i = Image(i.sequence[0]).clone() # first frame
                width, height, resolution, size = pic_component(i,image)
                now = datetime.datetime.today()
                new_img = i.clone()
                cursor.execute("insert into NewMedia (data_owner, url_of_data,picture_type,picture_resolution,date,width,height,Size_in_Bytes,InsertDate,Checked_Date) Values(?,?,?,?,?,?,?,?,?,?)" ,(data_owner, db_url.decode("utf8"), 'pic', resolution, Insert_Date, width, height, size, Insert_Date, now))
                conn.commit()
                cursor.execute('select @@identity')
                thumb = int(cursor.fetchone()[0])
                if (height >= 100 or width >= 100):
                    regular_size = resize_pic(height,width,250,new_img,str(thumb) + '.jpg', GCS)
                    big_size = None
                    #if(height >= 400 or width >= 400): # save big_pic
                    #    big_size = resize_pic(height,width,600,i,str(thumb) + '_big_pic.jpg', GCS)
                    #    count_pic = 2
                    cursor.execute("update NewMedia set file_size_120 = ? , file_size_250 = ? , Data_Type = ?  where id = ? " ,regular_size, big_size, 'Pic', thumb)
                    conn.commit()
                    return thumb, count_pic
                else:
                    # small pic
                    cursor.execute("update NewMedia set Data_Type = ? where id = ?" ,'Small_Pic', thumb)
                    conn.commit()
                    return thumb, 0
        except:
            # unavailable pic
            return data_owner,-1
    else:
        # unavailable url
        return data_owner,image

def upload_and_delete(newStr, Insert_Date,db_url):
    """
    :param newStr: url
    :param newStr: Insert Date

    the function uploads all the pics that was created at download_and_save function from GCS directory.
    if the num_of_pic = 2 it creates 2 pics (big and regular), if the num_of_pic = 1 it creates only regular pic
    in both of the cases, the function delete the pics from the computer after save them at Google Cloud Storage

    if the num_of_pic = 0 marks small pic
    if num_of_pic = -1 marks unavailable_url.
    else represent blurry_pic
    """
    print ('time =' +str(datetime.datetime.today()) +  ', link = ' + db_url)
    parm, num_of_pic = download_and_save(newStr,Insert_Date,db_url)
    if num_of_pic > 0:
        upload_file_to_bucket(GCS, 'easy/images/PICS', Google)
        #if num_of_pic == 2:
        #     os.remove(GCS + str(parm) + '_big_pic.jpg')
        os.remove(GCS + str(parm) + '.jpg')
        return
    elif num_of_pic == 0:
        return
    else:
        print ('UNAVAILABLE_URL')
        now = datetime.datetime.today()
        if num_of_pic == -1:
            data = 'unavailable_url'
        else:
            data = 'blurry_pic'
        cursor.execute("insert into NewMedia (data_owner,url_of_data,Data_Type, picture_type,picture_resolution,date,Size_in_Bytes,InsertDate,Checked_Date) Values(?,?,?,?,?,?,?,?,?)" ,(parm,db_url.decode("utf8"),data,'pic','0x0',now ,'', Insert_Date, now))
        conn.commit()
        cursor.execute('select @@identity')
        #return int(cursor.fetchone()[0])

def insert_datas_to_newmedia_table(sql):
    """
    :param sql: marks the Table from DB (Datas or StaticDatas)
    this function finds all the pics in Datas/StaticDatas and not in NewMedia.
    """
    try:
        for row in cursor.execute(sql).fetchall():
            check = cursor.execute(SpiderManager).fetchall()
            if check[0][0] == False:
                try:
                    time.sleep(3)
                    db_url = row[0].encode('UTF-8', 'ignore')
                    newStr = (urllib.quote(db_url,safe="%/:=&?~#+!$,;'@()*[]" )).decode("utf8")
                    if db_url.find("http://easy.co.il/tempuploads/") != -1:
                        newStr = newStr.replace("easy.co.il","37.19.113.41")
                    upload_and_delete(newStr,row[1],db_url)
                except:
                    print ("failed insert_datas_to_newmedia_table")
            else:
                time.sleep(1000)
    except:
        print ("not connected to sql")

def main():
    while True:
        check = cursor.execute(SpiderManager).fetchall()
        if check[0][0] == False:
            os.system('gsutil ls gs://easy/images/PICS/10000*')
            try:
                insert_datas_to_newmedia_table(datas)
    #            insert_datas_to_newmedia_table(staticDatas)
                time.sleep(1000)
            except:
                print ("failed")
        else:
            time.sleep(3000)
    shutil.rmtree(GCS) #delete temp folder
    shutil.rmtree(Google)

main()







